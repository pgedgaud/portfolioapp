
export class GameOfLife {
 
    constructor() { 

    }
    
    attached() {
        this.gridHeight = 100;
        this.gridWidth = 50;
        this.run = false;
        this.theGrid = [];
        this.drawToCanvas = true;
        this.drawToDiv = false;
        this.drawToConsole = false;
        this.isRunning = false;
        //need if to check that canvas is passed
        this.heading = 'Game Of Life';
        
        this.canvas = document.getElementById("gameCanvas");
        this.canvas.addEventListener('click', (evt) => this.CanvasClick(evt), false);
        this.canvasContext = this.canvas.getContext("2d");
        
        var x;
        for (x = 0; x < this.gridHeight; x++) {
            this.theGrid[x] = [];
        }
        
        this.SeedGrid();
        this.DrawCycle();

    }
     
    GenerateNewGame(){
        this.ClearCanvas();   
        var x, y;
        for (x = 0; x < this.gridHeight; x++) {
            for (y = 0; y < this.gridWidth; y++) {
                var ranNum = Math.random();
                if(ranNum > 0.5){
                     this.theGrid[x][y] = 0;     
                }
                else {
                    this.theGrid[x][y] = 1;
                }
            }
        } 
        
        this.DrawCycle();
    }
    
    ClearCanvas(){
        var x, y;
        for (x = 0; x < this.gridHeight; x++) {
            for (y = 0; y < this.gridWidth; y++) {
                this.theGrid[x][y] = 0;
            }
        }
        
        this.DrawCycle();
    }
    
    CanvasClick(evt) {
        var mousePos = this.GetMousePos(this.canvas, evt);
        this.theGrid[mousePos.xPos][mousePos.yPos] = 1;
        this.DrawCycle();
    }
    
    PausePlay() {
        if(this.isRunning === true) {
            this.Pause();
            this.isRunning = false;
        }
        else{
            this.RunGame();
            this.isRunning = true;
        }
    }
    
    RunGame() {
       
        this.interval = setInterval(() => this.Step(), 1000);
    }

    Pause() {
        clearInterval(this.interval);
    }
    
    IsCellAlive(x, y) {
        if (x > this.theGrid.length || x < 0) {
            return false;
        } else {
            if (y > this.theGrid[x].length || y < 0) {
                return false;
            } else {
//                console.log(this.theGrid[x][y] === 1);
                return this.theGrid[x][y] === 1;
            }
        }
    }
    
    FindNumOfNeighbors(x, y) {
        var num = 0, j, k;
        
        for (j = -1; j < 2; j++) {
            for (k = -1; k < 2; k++) {
                if (!(j === 0 && k === 0) && (x + j) > 0 && (y + k) > 0) {
                    if ( (x + j) < this.theGrid.length && (y + k) < this.theGrid[x + j].length) {
                        if (this.IsCellAlive((x + j), (y + k))) {
                            num++;
                        }
                    }
                }
            }
        }
        
        return num;
    };
    
    CalculateNextCycle() {      
        var x, y, i, j, newCells = [], num;
        
        for (i = 0; i < this.gridHeight; i++) {
            newCells[i] = [];
            
            for (j = 0; j < this.gridWidth; j++) {
                newCells[i][j] = 0;
            }
        }
        
        for (x = 0; x < this.gridHeight; x++) {
            for (y = 0; y < this.gridWidth; y++) {
                
                num = this.FindNumOfNeighbors(x, y);
                
                if (this.theGrid[x][y] === 1) {
                    if (num === 2 || num === 3) {
                        newCells[x][y] = 1;
                    }
                } else {
                    if (num === 3) {
                        newCells[x][y] = 1;
                    }
                }
            }
        }
        
        this.theGrid = newCells;
    }
    
    DrawCycle() {
        var x, y, outputStr = '';
        
        if (this.drawToConsole) {
            for (x = 0; x < this.theGrid.length; x++) {
                outputStr = '';
                
                for (y = 0; y < this.theGrid[x].length; y++) {
                    if (this.theGrid[x][y] === 0) {
                        outputStr += ' ';
                    } else {
                        outputStr += '#';
                    }
                }
                
                console.log(outputStr);
            }
        } else if (this.drawToCanvas) {
            this.canvasContext.clearRect(0, 0, this.canvas.width, this.canvas.height);
            
            
            for (x = 0; x < this.theGrid.length; x++) {
                this.canvasContext.beginPath();
                this.canvasContext.moveTo((x * 10), 0);
                this.canvasContext.lineTo((x * 10), this.canvas.height);
                this.canvasContext.lineWidth = 0.5;
                this.canvasContext.strokeStyle = "#9a9a9a";
                this.canvasContext.stroke();
                
                for (y = 0; y < this.theGrid[x].length; y++) {
                    this.canvasContext.beginPath();
                    this.canvasContext.moveTo(0, (y * 10));
                    this.canvasContext.lineTo(this.canvas.width, (y * 10));
                    this.canvasContext.lineWidth = 0.5;
                    this.canvasContext.strokeStyle = "#9a9a9a";
                    this.canvasContext.stroke();
                    
                    if (this.theGrid[x][y] === 1) {
                        this.canvasContext.fillStyle = "#FF0000";
                        this.canvasContext.fillRect((x * 10),(y * 10), 10, 10);
                    }
                }
            }
        }
    }
    
    RunLifeCycle() {
        var interval = window.setInterval(this.Step(), 1000);
        return interval;
    }
    
    StepButtonClick(){
        if(this.isRunning === false) {
            this.Step();    
        }
    }
    
    Step() {
        this.CalculateNextCycle();
        this.DrawCycle();
    }
    
    GetMousePos(canvas, evt) {
        var rect = this.canvas.getBoundingClientRect();
        var x = evt.clientX - rect.left;
        var y = evt.clientY - rect.top;
        var width = canvas.width;
        var height = canvas.height;
        return {xPos: Math.floor(x/(width/100)), yPos: Math.floor(y/(height/50))};
    }
    
    SeedGrid() {
        this.ClearCanvas();
        this.theGrid[5][4] = 1;
        this.theGrid[6][4] = 1;
        this.theGrid[4][5] = 1;
        this.theGrid[5][5] = 1;
        this.theGrid[5][6] = 1;
    }
}