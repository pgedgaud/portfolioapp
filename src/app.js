export class App {
  configureRouter(config, router) {
    config.title = '<Pg>';
    config.map([
        { route: ['','welcome'], name: 'welcome', moduleId: './welcome', nav: true, title:'Welcome' },
        { route: ['work'], name: 'work', moduleId: './work', nav: true, title:'Work' },
        { route: ['skills'], name: 'skills', moduleId: './skills', nav: true, title:'Skills' },
        { route: ['gameOfLife'], name: 'gameOfLife', moduleId: './gameOfLife', nav: true, title:'Game Of Life' }
    ]);

    this.router = router;
  }
}
