var unbundle = require('aurelia-bundler').unbundle;

var config = {
    force: true,
    baseURL: '.', // baseURL of the application
    configPath: './config.js', // config.js file. Must be within `baseURL`
    bundles: {
        "dist/app-build": { // bundle name/path. Must be within `baseURL`. Final path is: `baseURL/dist/app-build.js`.
            includes: [
        '[*.js]',
        '*.html!text',
        '*.css!text',
      ],
            options: {
                inject: true,
                minify: true
            }
        },
        "dist/vendor-build": {
            includes: [
        'aurelia-bootstrapper',
        'aurelia-fetch-client',
        'aurelia-router',
        'aurelia-animator-css',
        'aurelia-templating-binding',
        'aurelia-templating-resources',
        'aurelia-templating-router',
        'aurelia-loader-default',
        'aurelia-history-browser',
        'aurelia-logging-console',
        'bootstrap/css/bootstrap.css!text',
          'jquery',
          'bootstrap'
      ],
            options: {
                inject: true,
                minify: true
            }
        }
    }
};

unbundle(config);
